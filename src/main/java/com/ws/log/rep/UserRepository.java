package com.ws.log.rep;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ws.log.model.User;

public interface UserRepository extends JpaRepository<User, Long>{

	User getByEmail(String username);

	User findByEmail(String email);


}
