package com.ws.log.rep.elasticsearch;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ws.log.model.User;

public interface UserRepElasticsearch extends ElasticsearchRepository<User, String> {

    Page<User> findByEmail(String email, Pageable pageable);

    List<User> findBylastName(String title);

}
