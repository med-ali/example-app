package com.ws.log.model;

public class SessionData {
	private String sessionId;
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	private Integer hight;
	private Integer width;
	public Integer getHight() {
		return hight;
	}
	public void setHight(Integer hight) {
		this.hight = hight;
	}
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public SessionData(Integer hight, Integer width) {
		super();
		this.hight = hight;
		this.width = width;
	}
	
}
