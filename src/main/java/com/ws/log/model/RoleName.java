package com.ws.log.model;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
