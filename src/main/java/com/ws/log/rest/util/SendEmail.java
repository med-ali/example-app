package com.ws.log.rest.util;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import com.sun.xml.messaging.saaj.packaging.mime.MessagingException;
import com.ws.log.rest.TestController;

public class SendEmail {
	@Autowired
    private static JavaMailSender javaMailSender;
	private final static Logger log = LoggerFactory.getLogger(TestController.class);
	public static void sendEmail(String email,String subject,String text) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo("medali.sarray@gmail.com");
        log.debug("REST request to search for a page of TvaCodes for query {}",
				msg.getText()+msg.getTo()+msg.getSubject());
        //msg.setTo(email);
        msg.setSubject("subject");
        msg.setText("text");
        javaMailSender.send(msg);
    }
	public static void sendmail(String email,String subject,String text) throws MessagingException, IOException, javax.mail.MessagingException {
		   Properties props = new Properties();
		   props.put("mail.smtp.auth", "true");
		   props.put("mail.smtp.starttls.enable", "true");
		   props.put("mail.smtp.host", "smtp.gmail.com");
		   props.put("mail.smtp.port", "587");
		   
		   Session session = Session.getInstance(props, new javax.mail.Authenticator() {
		      protected PasswordAuthentication getPasswordAuthentication() {
		         return new PasswordAuthentication("medali.sarray@gmail.com", "medalisarray2016");
		      }
		   });
		   Message msg = new MimeMessage(session);
		   msg.setFrom(new InternetAddress("medali.sarray@gmail.com", false));

		   msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
		   msg.setSubject(subject);
		   msg.setContent(text, "text/html");
		   msg.setSentDate(new Date());

		   /*MimeBodyPart messageBodyPart = new MimeBodyPart();
		   messageBodyPart.setContent("Tutorials point email", "text/html");

		   Multipart multipart = new MimeMultipart();
		   multipart.addBodyPart(messageBodyPart);
		   MimeBodyPart attachPart = new MimeBodyPart();

		   attachPart.attachFile("/var/tmp/image19.png");
		   multipart.addBodyPart(attachPart);
		   msg.setContent(multipart);*/
		   Transport.send(msg);   
		}
}
