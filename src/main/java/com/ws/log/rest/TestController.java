package com.ws.log.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ws.log.model.SessionData;
import com.ws.log.model.dto.LoginRequest;
import com.ws.log.model.dto.SignUpRequest;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class TestController {
	private final Logger log = LoggerFactory.getLogger(TestController.class);
	public List<SessionData> sessionDataList = new ArrayList<SessionData>();
	@GetMapping("/testproject/test")
	public ResponseEntity<List<String>> test() {
		
		Integer totalRow = 12;
		HttpHeaders headers = new HttpHeaders();
        headers.add("X-Total-Count", String.valueOf(totalRow));
		List<String> pageData = new ArrayList<String>();
		return new ResponseEntity<>(pageData, headers, HttpStatus.OK);
	}
	@GetMapping("/testproject")
	public ResponseEntity<List<String>> testp() {
		
		Integer totalRow = 12;
		HttpHeaders headers = new HttpHeaders();
        headers.add("X-Total-Count", String.valueOf(totalRow));
		List<String> pageData = new ArrayList<String>();
		return new ResponseEntity<>(pageData, headers, HttpStatus.OK);
	}
	@PostMapping("/savesession")
	public ResponseEntity<List<SessionData>> savesession(
			@RequestBody SessionData sessionData) {
		HttpHeaders headers = new HttpHeaders();
		if (sessionDataList.size() != 0 && sessionDataList != null) {
			for (SessionData e : sessionDataList) {
				log.debug("REST request to search for a page of TvaCodes for query {}",
						e.getSessionId());
				if (e.getSessionId().equals(sessionData.getSessionId())){
					this.sessionDataList.remove(e);
					break;
				}
			}
		}
		sessionDataList.add(sessionData);
		/*Integer totalRow = 12;
		HttpHeaders headers = new HttpHeaders();
        headers.add("X-Total-Count", String.valueOf(totalRow));
		*/
		return new ResponseEntity<>(sessionDataList, headers, HttpStatus.OK);
	}
	@GetMapping("/getsessiondata")
	public ResponseEntity<SessionData> getsessiondata(
			@RequestParam(value = "sessionId",required = true) String sessionId) {
		HttpHeaders headers = new HttpHeaders();
		SessionData res = null ;
		log.debug("REST request to search for a page of TvaCodes for query {}",
				this.sessionDataList.size());
		for (SessionData e : this.sessionDataList) {
			log.debug("REST request to search for a page of TvaCodes for query {}",
					e.getSessionId());
			if (e.getSessionId().equals(sessionId)){
				res = e;
				break;
			}
		}
		/*sessionDataList.forEach(e -> {
			if (e.getSessionId() == sessionId){
				res = e;
			}
		});
		Integer totalRow = 12;
		HttpHeaders headers = new HttpHeaders();
        headers.add("X-Total-Count", String.valueOf(totalRow));
		*/
		return new ResponseEntity<>(res, headers, HttpStatus.OK);
	}
}
