package com.ws.log.rest;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.sun.xml.messaging.saaj.packaging.mime.MessagingException;
import com.ws.log.config.JwtTokenProvider;
import com.ws.log.exception.AppException;
import com.ws.log.model.Role;
import com.ws.log.model.RoleName;
import com.ws.log.model.User;
import com.ws.log.model.dto.JwtAuthenticationResponse;
import com.ws.log.model.dto.JwtRequest;
import com.ws.log.model.dto.JwtResponse;
import com.ws.log.model.dto.JwtResponseLogin;
import com.ws.log.model.dto.LoginRequest;
import com.ws.log.model.dto.SignUpRequest;
import com.ws.log.model.dto.UserPrincipal;
import com.ws.log.rep.RoleRepository;
import com.ws.log.rest.util.ApiResponse;
import com.ws.log.rest.util.SendEmail;
import com.ws.log.service.CustomUserDetailsService;
import com.ws.log.service.UserService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class JwtAuthenticationController {
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private JwtTokenProvider jwtTokenUtil;
	@Autowired
	private CustomUserDetailsService userDetailsService;
	@Autowired
	private UserService userService;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
    PasswordEncoder passwordEncoder;
	@Autowired
	JwtTokenProvider tokenProvider;
	private final Logger log = LoggerFactory.getLogger(TestController.class);
	@PostMapping("/authenticate")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) throws MessagingException, IOException, javax.mail.MessagingException {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        UserPrincipal user = tokenProvider.getUser(authentication);
        HashMap<String, Serializable> re = new HashMap<String, Serializable>();
        re.put("jwtToken", jwt);
        re.put("user", user);
        //SendEmail.sendEmail(loginRequest.getEmail(), "login", "you login");
        SendEmail.sendmail(loginRequest.getEmail(), "login", "you login");
        return ResponseEntity.ok(re);
    }
	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
	@PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        if(userService.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, "Email is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        // Creating user's account
        User user = new User(null,signUpRequest.getFirstName(), signUpRequest.getLastName(),
                signUpRequest.getEmail(), signUpRequest.getPassword());

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new AppException("User Role not set."));

        user.setRoles(Collections.singleton(userRole));

        User result = userService.save(user);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/users/{username}")
                .buildAndExpand(result.getFirstName()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
    }
	@PutMapping("/updatepassword")
    public ResponseEntity<?> updatePassword(@Valid @RequestBody LoginRequest loginRequest) {
		User user = userService.findByEmail(loginRequest.getEmail());
		if(user == null) {
            return new ResponseEntity(new ApiResponse(false, "Email not exist!"),
                    HttpStatus.BAD_REQUEST);
        }
		user.setPassword(passwordEncoder.encode(loginRequest.getPassword()));
		User userupdated = userService.save(user);
		URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/users/{username}")
                .buildAndExpand(user.getEmail()).toUri();
		return ResponseEntity.created(location).body(new ApiResponse(true, "password updated successfully"));
	}
}
