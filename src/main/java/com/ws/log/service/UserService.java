package com.ws.log.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ws.log.model.User;
import com.ws.log.rep.UserRepository;
import com.ws.log.rep.elasticsearch.UserRepElasticsearch;


@Service
@Transactional
public class UserService {
	private final Logger log = LoggerFactory.getLogger(UserService.class);
	@Autowired
	private  UserRepository userRepository;
	@Autowired
	private  UserRepElasticsearch userRepositoryElastic;
	public User save(User user) {
		log.debug("Request to save User : {}", user);
		//article.isNormalised(false);
		userRepositoryElastic.save(user);
		User result = userRepository.save(user);
		return result;
		//test to sarray branch
	}
	
	public boolean existsByEmail(String emial) {
		User user = userRepository.getByEmail(emial);//add test 2 
		if (user != null) {
			return true;
		}
		return false;
	}
	public User findByEmail(String emial) {
		User user = userRepository.getByEmail(emial);	 
		return user;
	}
}
